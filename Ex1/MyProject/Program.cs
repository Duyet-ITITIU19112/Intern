﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Nhap so luong giao vien: ");
        int soLuong = Convert.ToInt32(Console.ReadLine());

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < soLuong; i++)
        {
            Console.WriteLine($"\nNhap thong tin cho giao vien thu {i + 1}:");
            Console.Write("Ho ten: ");
            string hoTen = Console.ReadLine();

            Console.Write("Nam sinh: ");
            int namSinh = Convert.ToInt32(Console.ReadLine());

            Console.Write("Luong co ban: ");
            double luongCoBan = Convert.ToDouble(Console.ReadLine());

            Console.Write("He so luong: ");
            double heSoLuong = Convert.ToDouble(Console.ReadLine());

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }

        double luongThapNhat = double.MaxValue;
        GiaoVien giaoVienLuongThapNhat = null;

        foreach (GiaoVien giaoVien in danhSachGiaoVien)
        {
            if (giaoVien.TinhLuong() < luongThapNhat)
            {
                luongThapNhat = giaoVien.TinhLuong();
                giaoVienLuongThapNhat = giaoVien;
            }
        }

        Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
        giaoVienLuongThapNhat.XuatThongtin();

        Console.WriteLine("\nNhan mot phim bat ky de thoat...");
        Console.ReadKey();
    }
}
